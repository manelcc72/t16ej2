package com.manelcc.t16ej2;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnHabilitado, btnNombre, btnDireccion;
    private TextView txtHabilitado, txtNombre, txtDireccion;
    private BluetoothAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnHabilitado = (Button) findViewById(R.id.btnHabilitado);
        btnNombre = (Button) findViewById(R.id.btnNombre);
        btnDireccion = (Button) findViewById(R.id.btnDireccion);
        txtHabilitado = (TextView) findViewById(R.id.txtHabilitado);
        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtDireccion = (TextView) findViewById(R.id.txtDireccion);


        adapter = BluetoothAdapter.getDefaultAdapter();

        btnDireccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDireccion.setText(adapter.getAddress());
            }
        });

        btnNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNombre.setText(adapter.getName());
            }
        });

        btnHabilitado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHabilitado.setText(adapter.isEnabled() ? "Bluetooh activado" : "Bluetooh desactivado");
            }
        });

        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Toast.makeText(this, adapter.isEnabled() ? "Bluetooh activado" : "Bluetooh desactivado", Toast.LENGTH_LONG).show();
        }
    }
}
